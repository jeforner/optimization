f = @(x1, x2) 100*(x2 - x1^2)^2 + (1 - x1)^2; % Rosenbrock function
g = @(x1, x2) [-400*x1*(x2 - x1^2) - 2*(1 - x1);
               200*(x2 - x1^2)]; % gradient
h = @(x1, x2) [-400*(x2 - x1)^2 + 800*x1^2 + 2, -400*x1;
               -400*x1, 200]; % hessian      
x1 = 1.2; % starting x1 value
x2 = 1.2; % starting x2 value
           
answer = TRDogleg(f, g, h, x1, x2);