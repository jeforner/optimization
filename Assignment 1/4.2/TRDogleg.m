function minimum = TRDogleg(f, g, B, x1, x2)
    deltahat = 5; % Max trust-region radius
    delta = 2 ; % Initial trust region radius
    eta = .1; % Threshold value
    maxit = 100; % Maximum number of iterations
    k = 0; % Iteration counter
    
    while norm(g(x1, x2)) >= 1e-10 && k <= maxit % convergence criteria
        fprintf('%2d \t %12.6f %12.6f \n', k, x1, x2);
        % 1. Finding the search direction p
        pB = -inv(B(x1, x2))*g(x1, x2); % Full step
        if norm(pB) <= delta % If the full step is less than or equal to the trust region radius
            p = pB; % Use the full step
        else 
            pU = -((g(x1, x2)'*g(x1, x2))/(g(x1, x2)'*B(x1, x2)*g(x1, x2)))*g(x1, x2); % Unconstrained min along -g
            if norm(pU) >= delta % If the steepest descent direction lies outside of the trust region
                p = delta*(pU/norm(pU)); % Use a scaled version of the steepest descent direction
            else
                tau = max(roots(norm(pB - pU)^2, 2*pU'*(pB - pU), norm(pU)^2 - delta^2)); % Calculating tau to be such that ||p|| = delta
                p = pU + tau*(pB - pU); % Use a search direction along the path from pU to pB   
            end
        end   
        p1 = p(1);
        p2 = p(2);
        m = @(p1, p2) f(x1, x2) + g(x1, x2)'*[p1; p2] + .5*[p1; p2]'*B(x1, x2)*[p1; p2]; % Model of the objective function
        
        % 2. Evaluating the ratio of actual reduction to predicted
        % reduction, and finding the next iterate x
        rho = (f(x1, x2) - f(x1 + p1, x2 + p2))/(m(0, 0) - m(p1, p2));
        if rho < .3
            delta = .3*delta; % Shrink the trust region radius  
        elseif rho > .7 && norm(p) == delta
                delta = min(2*delta, deltahat);
        end
        if rho > eta
            x1 = x1 + p1; % New x1 value
            x2 = x2 + p2; % New x2 value
        end
        k = k + 1; % Incrementing counter
    end
    minimum = f(x1, x2); % Approximate value of a minimum of f
    fprintf('After %d iterations, we find an approximate minimum of %.10f at the point (x1, x2) = (%.10f, %.10f). \n', k, minimum, x1, x2)
end