n = 5;
A1 = zeros(n, n) + diag([.9 .95 1.1 4 5]); % Matrix A
b = ones(n, 1); % RHS vector b
x = zeros(n, 1); % Starting point

answer1 = CG(A1, x, b, n);

n = 5;
A2 = zeros(n, n) + diag(1:n); % Empty matrix A
b = ones(n, 1); % RHS vector b
x = zeros(n, 1); % Starting point

answer2 = CG(A2, x, b, n);


x1 = [1.1111, 1.0526, 0.9091, 0.2500, 0.2000]'; % Solution in answer1
x2 = [1.0000, 0.5000, 0.3333, 0.2500, 0.2000]'; % Solution in answer2
diff1 = zeros(5, 5); % Initial matrix of vector differences for iterates in answer1
diff2 = zeros(5, 5); % Initial matrix of vector differences for iterates in answer2
y1 = zeros(1, n);
y2 = zeros(1, n);

for k = 1 : 5
    diff1(:, k) = answer1(:, k) - x1;
    diff2(:, k) = answer2(:, k) - x2;
    y1(k) = log(diff1(:, k)'*A1*diff1(:, k));
    y2(k) = log(diff2(:, k)'*A2*diff2(:, k));
end

it = 1 : 5;
plot(it, y1, 'b')
hold on
plot(it, y2, 'g')

title('Comparison of Convergence Rates with Different Eigenvalue Distributions')
xlabel('iteration')
ylabel('log(||x - xstar||^2)')
legend('clustered eigenvalues', 'uniformly distributed eigenvalues')