f = @(x1, x2) 100*(x2 - x1^2)^2 + (1 - x1)^2; % function of interest
g = @(x1, x2) [-400*x1*(x2 - x1^2) - 2*(1 - x1);
               200*(x2 - x1^2)]; % gradient
           
a = 1; % initial step length
rho = .5; % contraction factor
c = .5; % constant
x1 = -1.2; % initial x1 value
x2 = 1; % initial x2 value

answer = SD(f, g, a, rho, c, x1, x2);