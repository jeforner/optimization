function minimum = SD(f, g, a, rho, c, x1, x2)

p = @(x1, x2) -g(x1, x2); % search direction
b = a*p(x1, x2); % step vector
k = 0; % iteration counter

    while norm(g(x1, x2)) >= 10e-5 && k < 1e6 % stopping critetia
       
        while f(x1 + b(1), x2 + b(2)) > f(x1, x2) + c*a*g(x1, x2)'*p(x1, x2) % sufficient decrease condition
            a = rho*a; % shrinking "a"
            b = a*p(x1, x2); % recalculated step vector
        end
        
        fprintf('%2d \t %12.6f \n', k, a);
    
        d = a*p(x1, x2); % step vector using the value of "a" obtained in the above while loop
        x1 = x1 + d(1); % new x1 value
        x2 = x2 + d(2); % new x2 value
        a = 1; % resetting "a"
        k = k + 1; % next iteration
    end
    
    minimum = f(x1, x2); % answer
    fprintf('After %d iterations, we find an approximate minimum of %.10f at the point (x1, x2) = (%.10f, %.10f). \n', k, minimum, x1, x2)
end