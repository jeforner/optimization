function minimum = NM(f, g, h, a, rho, c, x1, x2)

p = @(x1, x2) - inv(h(x1, x2))*g(x1, x2); % Search direction with Newton's method
b = a*p(x1, x2); % Step vector
k = 0; % Iteration counter

    while norm(g(x1, x2)) >= 10e-5 && k < 1e6 % Stopping criterion
        
        while f(x1 + b(1), x2 + b(2)) > f(x1, x2) + c*a*g(x1, x2)'*p(x1, x2) % Sufficient decrease condition
            a = rho*a; % Shrinking the step size
            b = a*p(x1, x2); % Recalculating the step vector
        end
        
        fprintf('%2d \t %12.6f \n', k, a); % Keeps track of the iteration and the size of the step length "a" at each iteration
    
        d = a*p(x1, x2); % Step vector obtained from above for loop
        x1 = x1 + d(1); % new x1 value
        x2 = x2 + d(2); % new x2 value
        a = 1; % Resetting a
        k = k + 1; % Next iteration
    end
    
    minimum = f(x1, x2); % Answer
    fprintf('After %d iterations, we find an approximate minimum of %.10f at the point (x1, x2) = (%.10f, %.10f). \n', k, minimum, x1, x2)
end