function [x, xstar] = CG(A, x, b, n)
    
    r = zeros(n, n); % Initial residual matrix
    r(:, 1) = A*x - b; % Initial residual
    p = - r(:, 1); % Initial search direction
    x(:, 1) = x; % Starting point
    k = 1; % Iteration counter
    
    while norm(r(:, k)) > 1e-6
        
        a = r(:, k)'*r(:, k)/(p'*A*p); % Calculating alpha
        x(:, k + 1) = x(:, k) + a*p; % Next iterate
        r(:, k + 1) = r(:, k) + a*A*p; % Next residual
        b = r(:, k + 1)'*r(:, k + 1)/(r(:, k)'*r(:, k)); % Calculating beta
        p = -r(:, k + 1) + b*p; % New search direction
        k = k + 1; % Incrementing counter
        
    end 
    
    xstar = x(:, k);
    disp(xstar);

end