n = 5; %8, 12, 20
A = zeros(n, n); % Empty matrix A
b = ones(n, 1); % RHS vector b
x = zeros(n, 1); % Starting point


% Filling in entries to the Hilbert matrix A
for i = 1 : n
    for j = 1 : n
        A(i, j) = 1/(i + j - 1);
    end
end

answer = CG(A, x, b, n);

s = size(answer); % Size of the matrix containing the iterates "x" as its columns
S = s(2) - 1;  % Number of iterations required to reduce the residual to below 10^-6

fprintf('It takes %d iterations to reduce the residual to below 10^-6.', S)