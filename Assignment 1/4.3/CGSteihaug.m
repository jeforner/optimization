function p = CGSteihaug(epsilonk, n)

    % Make Bk the exact hessians
    
    z = 0;
    r0 = g;
    d = -r0;
    
    if norm(r0) < epsilonk
        p = z;
    end
    
    for j = 1 : n
        if d'*B*d <= 0
            % Find tau
            % Return pk
        end
        
        a = rj'*rj/d'*B*d;
        z = z + a*d;
        
        if norm(z) >= delta
            % Find tau
            % Return pk
        end
        
        r(j + 1) = rj + a*B*d;
        
        if norm(r(j + 1)) < epsilon
            p = z;
        end
        
        b = r(:, j + 1)'*r(:, j + 1)/(r(:, j)'*r(:, j));
        d = -r(:, j +1 ) + b*d;
        
    end
end