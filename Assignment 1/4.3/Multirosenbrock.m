function F = Multirosenbrock(x)
    
N = size(x,2); % assumes x is a row vector or 2-D matrix
if mod(N,2) % if N is odd
    error('Input rows must have an even number of elements')
end

odds  = 1 : 2 : N-1;
evens = 2 : 2 : N;
F = zeros(size(x));
F(odds)  = 1 - x(odds);
F(evens) = 10*(x(evens) - x(odds).^2);
F = sum(F.^2, 2);

end