N = 20; % any even number
mf = 200; % max fun evals
ub = 3*ones(1,N);
lb = -ub;
rng default
x0 = -3*rand(1,N);

test = Multirosenbrock(x0);