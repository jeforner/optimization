function xstar = activeqp(x0, W0)
    
    maxit = 100; % maximum number of iterations
    
    for k = 1 : maxit
        
        % Solve (16.39)to find pk;
        
        if p ~=0
            
            % Compute Lagrange multipliers that satisfy (16.42), with What
            % = W;
            
            if 
                break
                xstar = xk;
            else
                j = argmin;
                x(k + 1) = x(k);
                W(k + 1) = W(k)\j;
            
        else
            % Compute a(k) from (16.41);
            x(k + 1) = x(k) + a(k)*p(k);
            
            if there are blocking constraints
                % Obtain W(k + 1) by adding one of the blocking
                % constraints to W(k);
                
            else
                W(k + 1) = W(k);
            end
        end
    end
end